let studentsList=[];

function addStudent(name){
	studentsList.push(name);
	console.log(name+" was added to the student's List");	
}
function countStudents(){
	console.log("There are a total of " +studentsList.length+" students enrolled");
}
function printStudents(){
	studentsList.sort();
	studentsList.forEach(function(student){
	 	console.log(student);
	 });
}
function findStudent(search){	
	let filteredStudents = studentsList.filter(function(students){
		return students.toLowerCase().includes(search.toLowerCase());
	});
	if(filteredStudents.length==1){
		console.log(filteredStudents+ " is an Enrollee");
	}else if(filteredStudents.length>1){
		console.log(filteredStudents+ " are enrollees");
	}else{
		console.log("No Student found with the name "+search);
	}
}
function addSection(section){
	let studentsSection = studentsList.map(function(studentName){
		return studentName+" - Section "+ section;
	});
		console.log(studentsSection);
}
function removeStudent(name){
	let newName = name.slice(0,1); 
	let sentName=newName.toUpperCase().concat(name.slice(1,name.length));
	let indexToRemove=studentsList.indexOf(sentName);
	studentsList.splice(indexToRemove, 1);
	console.log(name+ " was remove from the student's list");
}



